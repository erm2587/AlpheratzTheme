<?php get_header(); ?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="post-inner-content error">

				<section class="error-404 not-found">
					<header class="page-header">
						<h1 class="page-title"><?php _e( '¡Oh, no! 😮 ¡Maldición!' ); ?></h1>
					</header><!-- .page-header -->

					<div class="img404">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/404.png" alt="404 Error" title="404 Error" />
						<h3 class="h3error">¡Estamos siendo invadidos por fantasmas y no podemos mostrar el contenido que buscabas! 😔</h3>
					</div>

					<div class="page-content">
						<h4 class="h4error"><?php _e( 'Quizás este apuesto y seductor buscador te puede ayudar en algo... 😏' ); ?></h4>

						<?php get_search_form(); ?>
					</div><!-- .page-content -->

				</section><!-- .error-404 -->
			</div><!-- .post-inner-content -->
		</main><!-- #main -->
	</div><!-- #primary -->
<?php get_footer(); ?>
