<?php

/* FILES */
function NunkiEngineUpdate( $transient ) {
    if ( empty( $transient->checked ) ) {
        return $transient;
    }

    $theme_data = wp_get_theme(wp_get_theme()->template);
    $theme_slug = $theme_data->get_template();
    //Delete '-master' from the end of slug
    $theme_uri_slug = preg_replace('/-master$/', '', $theme_slug);

    $remote_version = '0.0.0';
    $style_css = wp_remote_get("https://gitlab.com/zagur/".$theme_uri_slug."/raw/master/style.css")['body'];
    if ( preg_match( '/^[ \t\/*#@]*' . preg_quote( 'Version', '/' ) . ':(.*)$/mi', $style_css, $match ) && $match[1] )
        $remote_version = _cleanup_header_comment( $match[1] );

    if (version_compare($theme_data->version, $remote_version, '<')) {
        $transient->response[$theme_slug] = array(
            'theme'       => $theme_slug,
            'new_version' => $remote_version,
            'url'         => 'https://gitlab.com/zagur/'.$theme_uri_slug,
            'package'     => 'https://gitlab.com/zagur/'.$theme_uri_slug.'/repository/archive.zip',
        );
    }
    return $transient;
}
add_filter( 'pre_set_site_transient_update_themes', 'NunkiEngineUpdate' );

// Theme Options
define( 'OPTIONS_FRAMEWORK_DIRECTORY', get_template_directory_uri() . '/inc/NunkiCore/' );
require_once dirname( __FILE__ ) . '/inc/NunkiCore/options-framework.php';
require_once dirname( __FILE__ ) . '/inc/NunkiCore/nunki-options.php';

// Loads options.php from child or parent theme
$optionsfile = locate_template( 'options.php' );
load_template( $optionsfile );

/* MAIN FUNCTIONS */

// Register Custom Navigation Walker and add menu
require_once('wp_bootstrap_navwalker.php');

register_nav_menus( array(
    'primary' => __( 'Menu Superior', 'Alpheratz Theme' ),
) );

// Active thumbnails
add_theme_support('post-thumbnails');
add_image_size('list_articles_thumbs', 350, 270, true );
the_post_thumbnail();

the_post_thumbnail('thumbnail');    // Thumbnail (default 150px x 150px max)
the_post_thumbnail('medium');       // Medium resolution (default 300px x 300px max)
the_post_thumbnail('large');        // Large resolution (default 640px x 640px max)
the_post_thumbnail('full');         // Original image resolution (unmodified)

//eliminar codigo basura de cabecera
/*
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'feed_links', 2);
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'start_post_rel_link', 10, 0);
remove_action('wp_head', 'parent_post_rel_link', 10, 0);
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);
//Automatically move JavaScript code to page footer, speeding up page loading time.
remove_action('wp_head', 'wp_print_scripts');
remove_action('wp_head', 'wp_print_head_scripts', 9);
remove_action('wp_head', 'wp_enqueue_scripts', 1);
add_action('wp_footer', 'wp_print_scripts', 5);
add_action('wp_footer', 'wp_enqueue_scripts', 5);
add_action('wp_footer', 'wp_print_head_scripts', 5);
*/



// Add sidebar
register_sidebar(array(
    'name' => 'SideBar',
    'before_widget' => '<div class="widget">',
    'after_widget' => '</div>',
    'before_title' => '<h3>',
    'after_title' => '</h3>'
));

// Add sidebar on footer
register_sidebar(array(
    'name' => 'Footer',
    'before_widget' => '<div class="widget col-md-4">',
    'after_widget' => '</div>',
    'before_title' => '<h3>',
    'after_title' => '</h3>'
));

// Add Social Network on profile
function alpheratzAuthorSocialIcons( $contactmethods ) {

    $contactmethods['twitter'] = 'Twitter URL';
    $contactmethods['facebook'] = 'Facebook URL';
    $contactmethods['googleplus'] = 'Google + URL';
    $contactmethods['correo'] = 'Correo Electrónico';
    $contactmethods['github'] = 'GitHub URL';
    /*$contactmethods['gnusocial'] = 'GNU Social URL';
    $contactmethods['diaspora'] = 'Diaspora* URL';*/

    return $contactmethods;
}
add_filter( 'user_contactmethods', 'alpheratzAuthorSocialIcons', 10, 1);

// Social Network in user profile
function alperatzListSocialNetwork() {

    echo '<ul class="icons">';
    $twitter = get_the_author_meta( 'twitter' );
    if ( $twitter && $twitter != '' ) {
        echo '<li><a target="_blank" class="icon-social tw" href="' . esc_url($twitter) . '"><i class="fa fa-twitter"></i></a></li>';
    }

    $facebook = get_the_author_meta( 'facebook' );
    if ( $facebook && $facebook != '' ) {
        echo '<li><a target="_blank" class="icon-social fb" href="' . esc_url($facebook) . '"><i class="fa fa-facebook"></i></a></li>';
    }

    $googleplus = get_the_author_meta( 'googleplus' );
    if ( $googleplus && $googleplus != '' ) {
        echo '<li><a target="_blank" class="icon-social gp" href="' . esc_url($googleplus) . '"><i class="fa fa-google-plus"></i></a></li>';
    }

    $correo = get_the_author_meta( 'correo' );
    if ( $correo && $correo != '' ) {
        echo '<li><a target="_blank" class="icon-social ce" href="mailto:' . esc_url($correo) . '"><i class="fa fa-envelope"></i></a></li>';
    }

    $github = get_the_author_meta( 'github' );
    if ( $github && $github != '' ) {
        echo '<li><a target="_blank" class="icon-social gh" href="' . esc_url($github) . '"><i class="fa fa-github-alt"></i></a></li>';
    }

    /*$gnusocial = get_the_author_meta( 'gnusocial' );
    if ( $gnusocial && $gnusocial != '' ) {
        echo '<li><a target="_blank" class="gs" href="' . esc_url($gnusocial) . '"><span class="sociconoff">GNUSocial</span></a></li>';
    }

    $diaspora = get_the_author_meta( 'diaspora' );
    if ( $diaspora && $diaspora != '' ) {
        echo '<li><a target="_blank" class="dp" href="' . esc_url($diaspora) . '"><span class="sociconoff">Diaspora</span></a></li>';
    }*/
    echo '</ul>';
}

function oldPosts() {

    // Variables, a year, 2 year, 3 year, 4 year, more than 5 years
    $time_old_year = 60*60*24*365;
    $time_old_2year = 60*60*24*365*2;
    $time_old_3year = 60*60*24*365*3;
    $time_old_4year = 60*60*24*365*4;
    $time_old_MoreYear = 60*60*24*365*5;
    $time_now = date('U')-get_the_time('U');

    if ($time_now > $time_old_year && $time_now < $time_old_2year) {
        echo '<div class="alert alert-success" role="alert">
            <strong>ATENCIÓN: Este artículo fue publicado hace un año.</strong><br />
                Es posible que este artículo contenga información desfasada.
            </div>';
    } elseif ($time_now > $time_old_2year && $time_now < $time_old_3year) {
        echo '<div class="alert alert-info" role="alert">
            <strong>ATENCIÓN: Este artículo fue publicado hace dos años.</strong><br />
                Es posible que este artículo contenga información desfasada.
            </div>';
    } elseif ($time_now > $time_old_3year && $time_now < $time_old_4year) {
        echo '<div class="alert alert-warning" role="alert">
            <strong>ATENCIÓN: Este artículo fue publicado hace tres años.</strong><br />
                Es posible que este artículo contenga información desfasada.
            </div>';
    } elseif ($time_now > $time_old_4year && $time_now < $time_old_MoreYear) {
        echo '<div class="alert alert-danger" role="alert">
            <strong>ATENCIÓN: Este artículo fue publicado hace cuatro años.</strong><br />
                Es posible que este artículo contenga información desfasada.
            </div>';
    } elseif ($time_now > $time_old_MoreYear) {
        echo '<div class="alert alert-black" role="alert">
            <strong>ATENCIÓN: Este artículo fue publicado hace más de cinco años.</strong><br />
                Es posible que este artículo contenga información desfasada.
            </div>';
    }
}

// Posts relacionados (en pruebas)
function AlpheratzRelatedAuthorPosts() {
    global $authordata, $post;
    $authors_posts = get_posts( array( 'author' => $authordata->ID, 'post__not_in' => array( $post->ID ), 'posts_per_page' => 6 ) );
    $output = '<ul>';
    foreach ( $authors_posts as $authors_post ) {
        $output .= '<li class><a href="' . get_permalink( $authors_post->ID ) . '">' . apply_filters( 'the_title', $authors_post->post_title, $authors_post->ID ) . '</a></li>';
    }
    $output .= '</ul>';
    return $output;
}

/**
 * Load javascripts used by the theme
 */

function AlpheratzInfiniteScrollJS(){
	wp_register_script( 'infinite_scroll',  get_template_directory_uri() . '/js/jquery.infinitescroll.min.js', array('jquery'),null,true );
	if( ! is_singular() ) {
		wp_enqueue_script('infinite_scroll');
	}
}
add_action('wp_enqueue_scripts', 'AlpheratzInfiniteScrollJS');

/**
 * Infinite Scroll
 */
function AlpheratzInfiniteScroll() {

    $stateCheckboxInfiniteScroll = of_get_option('infinitescroll');

    if ($stateCheckboxInfiniteScroll == 1) {
        if( ! is_singular() ) { ?>
    	<script>
    	var infinite_scroll = {
    		loading: {
    			img: "<?php echo get_template_directory_uri(); ?>/img/loading.gif",
    			msgText: "<?php _e( 'Cargando entradas...', 'custom' ); ?>",
    			finishedMsg: "<?php _e( 'Has llegado al final!', 'custom' ); ?>",
                animate : true // if the page will do an animated scroll when new content loads
    		},
    		"nextSelector":"li a.next",
    		"navSelector":".navigation",
    		"itemSelector":"article",
    		"contentSelector":"#main"
    	};
    	jQuery( infinite_scroll.contentSelector ).infinitescroll( infinite_scroll );
    	</script>
    	<?php
    	}
    }
}
add_action( 'wp_footer', 'AlpheratzInfiniteScroll',200 );

// Mostrar los posts populares en single-content

function popularPosts() {

    $mipost = $post;
    $args = array( 'numberposts' => 3, 'orderby' => 'rand', 'post_status' => 'publish', 'offset' => 1);
    $rand_posts = get_posts( $args );

    foreach ($rand_posts as $post) {

        $id = $post->ID;
        $title = $post->post_title;
        $titlePrint = substr($title, 0, 26) . "...";

        echo '<li class="trendingPost">';
        echo '<a data-toggle="tooltip" data-placement="bottom" href="' . get_permalink($id) . '" title="' . $title . '">' . $titlePrint . '</a> ';
        echo '</li>';
    }
}

/** THEME OPTIONS FUNCTIONS **/

















/*
set_site_transient('update_themes', null);
function AlpheratzThemeUpdate( $transient ) {
    if ( empty( $transient->checked ) ) {
        return $transient;
    }

    $theme_data = wp_get_theme(wp_get_theme()->template);
    $theme_slug = $theme_data->get_template();
    //Delete '-master' from the end of slug
    $theme_uri_slug = preg_replace('/-master$/', '', $theme_slug);

    $remote_version = '0.0.0';
    $style_css = wp_remote_get("https://gitlab.com/zagur/".$theme_uri_slug."/blob/master/style.css")['body'];
    if ( preg_match( '/^[ \t\/*#@]*' . preg_quote( 'Version', '/' ) . ':(.*)$/mi', $style_css, $match ) && $match[1] )
        $remote_version = _cleanup_header_comment( $match[1] );

    if (version_compare($theme_data->version, $remote_version, '<')) {
        $transient->response[$theme_slug] = array(
            'theme'       => $theme_slug,
            'new_version' => $remote_version,
            'url'         => 'https://gitlab.com/zagur/'.$theme_uri_slug,
            'package'     => 'https://gitlab.com/zagur/'.$theme_uri_slug.'/repository/archive.zip',
        );
    }
    return $transient;
}
add_filter( 'pre_set_site_transient_update_themes', 'AlpheratzThemeUpdate' );
*/

// Alpheratz Pagination
function AlpheratzPagination() {
    global $wp_query;
    $big = 999999999;
    $pages = paginate_links(array(
        'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
        'format' => '?page=%#%',
        'current' => max(1, get_query_var('paged')),
        'total' => $wp_query->max_num_pages,
        'prev_next' => false,
        'type' => 'array',
        'prev_next' => TRUE,
        'prev_text' => '&larr; Previous',
        'next_text' => 'Next &rarr;',
            ));
    if (is_array($pages)) {
        $current_page = ( get_query_var('paged') == 0 ) ? 1 : get_query_var('paged');
        echo '<ul class="pagination">';
        foreach ($pages as $i => $page) {
            if ($current_page == 1 && $i == 0) {
                echo "<li class='active'>$page</li>";
            } else {
                if ($current_page != 1 && $current_page == $i) {
                    echo "<li class='active'>$page</li>";
                } else {
                    echo "<li>$page</li>";
                }
            }
        }
        echo '</ul>';
    }
}

// Alpheratz Comments
function wp_bootstrap_comments($comment, $args, $depth) {
    $GLOBALS['comment'] = $comment; ?>
    <li <?php comment_class(); ?>>
		<article id="comment-<?php comment_ID(); ?>" class="clearfix comment-body">
			<div class="comment-author vcard clearfix">
				<div class="vcard col-sm-2">
					<?php echo get_avatar( $comment, $size='75' ); ?>
                </div>
				<div class="col-sm-10 comment-text">
					<?php printf('<h4>%s</h4>', get_comment_author_link()) ?>

                    <?php if ($comment->comment_approved == '0') : ?>
       					<div class="alert alert-info" role="alert">
                            <p><?php _e('Tu comentario está en la lista para ser moderado.','wpbootstrap') ?></p>
          				</div>
					<?php endif; ?>
                        <?php comment_text() ?>
                        <time datetime="<?php echo comment_time('Y-m-j'); ?>">
                            <a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>">
                                <?php comment_time('j F, Y - H:m'); ?>
                            </a>
                        </time>

					<?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
                    <?php edit_comment_link(__(' Editar '),'<span class="edit-comment"><span class="glyphicon glyphicon-edit"></span>','</span>') ?>

                </div>
			</div>
		</article>
    <!-- </li> is added by wordpress automatically -->
<?php
} // don't remove this bracket!

//Modificar los campos Autor, Email y Sitio web del formulario de comentarios
function apk_modify_comment_fields( $fields ) {

	//Variables necesarias para que esto funcione
    $commenter = wp_get_current_commenter();
	$req = get_option( 'require_name_email' );
	$aria_req = ( $req ? " aria-required='true'" : '' );

	$fields =  array(

	  'author' =>
	    '<input class="input-form-comment col-md-4" id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) .
	    '" size="30"' . $aria_req . ' placeholder="' . __('NOMBRE*', 'apk') . '" />', //Editamos el campo autor

	  'email' =>
	    '<input class="input-form-comment col-md-4 mig-form" id="email" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) .
	    '" size="30"' . $aria_req . ' placeholder="' . __('EMAIL*', 'apk') . '" />', //Editamos el campo email

	  'url' =>
	    '<input class="input-form-comment col-md-4" id="url" name="url" type="text" value="' . esc_attr( $commenter['comment_author_url'] ) .
	    '" size="30" placeholder="' . __('WEB', 'apk') . '"  />', //Editamos el campo sitio web
	);

	return $fields;
}
add_filter('comment_form_default_fields', 'apk_modify_comment_fields');

function wpsites_modify_comment_form_text_area($arg) {
    $arg['comment_field'] = '<textarea class="textarea-comment col-md-12" id="comment" name="comment" cols="45" placeholder="COMENTARIO*" rows="6" aria-required="true"></textarea>';
    return $arg;
}

add_filter('comment_form_defaults', 'wpsites_modify_comment_form_text_area');

// Display trackbacks/pings callback function (list_pings)
/*function AlpheratzListPings($comment, $args, $depth) {
       $GLOBALS['comment'] = $comment;
?>
        <li id="comment-<?php comment_ID(); ?>"><i class="icon icon-share-alt"></i>&nbsp;<?php comment_author_link(); ?>
<?php
}*/


// SHORTCODES
/*
function pathFileOnPostOn() {
    return '<code class="path">';
}
add_shortcode('pathOn', 'pathFileOnPostOn');

function pathFileOnPostOff() {
    return '</code>';
}
add_shortcode('pathOff', 'pathFileOnPostOff');
*/



?>
