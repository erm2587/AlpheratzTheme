<!-- Index. principal -->
<article id="post-<?php the_ID(); ?>" <?php post_class('col-md-4'); ?>>
	<div class="blog-item-wrap square">
		<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" alt="<?php the_title_attribute(); ?>" >
			<div class="thumb">
				<!-- Si tiene thumb por defecto, se muestra el thumb con tamaño "medium" (ver documentación en functions)
			 		 Si no tiene un thumb por defecto, entonces carga una imagen por defecto. -->
				<?php if ( has_post_thumbnail() ) {
					the_post_thumbnail('medium', array('class' => 'img-responsive'));
				} else { ?>
						<img class="img-responsive" src="<?php bloginfo('template_directory'); ?>/img/default-thumb.png" alt="<?php the_title(); ?>" />
				<?php } ?>
				<h1 class="entry-title">
					<?php the_title(); ?>
				</h1><!-- .entry-title -->
			</div><!-- img -->
		</a>
	</div><!-- square -->

	<div class="post-inner-content">
		<header class="entry-header page-header">

				<?php if ( 'post' == get_post_type() ) : ?>
					<div class="entry-meta row">
						<div class="post_date col-md-6 text-left">
							<span class="fa fa-calendar"></span>
							<?php the_time('d/m/Y') ?>
						</div>
						<div class="post_categories col-md-6 text-right">
							<span class="fa fa-tags"></span>

							<?php
								$category = get_the_category();
								if ($category) {
								  echo '<a href="' . get_category_link( $category[0]->term_id ) . '" title="' . sprintf( __( "View all posts in %s" ), $category[0]->name ) . '" ' . '>' . $category[0]->name.'</a> ';
								}
							?>

						</div>
						<?php edit_post_link( __( 'Edit' ), '<div class="edit-post"><span class="glyphicon glyphicon-edit"></span> <span class="edit-link">', '</span></div>' ); ?>

					</div><!-- .entry-meta -->
				<?php endif; ?>
			</header><!-- .entry-header -->

			<?php if ( is_search() ) : // Only display Excerpts for Search ?>
			<div class="entry-summary">
				<?php the_excerpt(); ?>
				<p><a title="<?php the_title_attribute(); ?>" alt="<?php the_title_attribute(); ?>" class="btn btn-default read-more" href="<?php the_permalink(); ?>"><?php _e( 'Leer más' ); ?></a></p>
			</div><!-- .entry-summary -->
			<?php else : ?>
				<!-- aquí es pot ficar contingut -->
			<?php endif; ?>
		</div>
</article><!-- #post-## -->
